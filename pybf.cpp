#include <vector>

#include <vexcl/vexcl.hpp>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace odeint = boost::numeric::odeint;
namespace py = pybind11;

//---------------------------------------------------------------------------
vex::Context& ctx(std::string name = "") {
    static vex::Context c(vex::Filter::Env && vex::Filter::Name(name));
    return c;
}

//---------------------------------------------------------------------------
enum VarNames {
    V_DEND  ,  // dend.V_dend
    V_SOMA  ,  // soma.V_soma
    V_AXON  ,  // axon.V_axon
    R_D     ,  // dend.Calcium_r
    Z_D     ,  // dend.Potassium_s
    N_D     ,  // dend.Hcurrent_q
    CA_CONC ,  // dend.Ca2Plus
    K_S     ,  // soma.Calcium_k
    L_S     ,  // soma.Calcium_l
    H_S     ,  // soma.Sodium_h
    N_S     ,  // soma.Potassium_n
    X_S     ,  // soma.Potassium_x_s
    H_A     ,  // axon.Sodium_h_a
    X_A        // axon.Potassium_x_a
};

//---------------------------------------------------------------------------
struct neuron_system {
    typedef vex::multivector<double, 14> state_type;

    odeint::euler/*runge_kutta4_classic*/<
        state_type, double, state_type, double,
        odeint::vector_space_algebra, odeint::default_operations
        > stepper;

    double Cm = 1;

    double p1 = 0.25;
    double p2 = 0.15;

    double g_int = 0.13;
    double g_cal_s = 0.68;
    double g_na_s = 150;
    double g_kdr_s = 9;
    double g_k_s = 5;
    double g_na_a = 240;
    double g_k_a = 20;

    double Vcah_d = 120;
    double Vkca_d = -75;
    double Vh_d = -43;
    double Vleak_d = 10;
    double Vcal_s = 120;
    double Vna_s = 55;
    double Vkdr_s = -75;
    double Vk_s = -75;
    double Vleak_s = 10;
    double Vna_a = 55;
    double Vk_a = -75;
    double Vleak_a = 10;

    double t = 0;
    double Iapp = 0;

    int n;

    vex::vector<double> g_leak_d;
    vex::vector<double> g_leak_s;
    vex::vector<double> g_leak_a;

    vex::vector<double> g_kca_d;
    vex::vector<double> g_cah_d;
    vex::vector<double> g_h_d;

    vex::multivector<double, 14> s;

    neuron_system(int n)
        : n(n), g_leak_d(ctx(),n), g_leak_s(ctx(),n), g_leak_a(ctx(),n),
          g_kca_d(ctx(),n), g_cah_d(ctx(),n), g_h_d(ctx(),n),
          s(ctx(),n)
    {
        g_leak_d = 0.016;
        g_leak_s = 0.016;
        g_leak_a = 0.016;
        g_kca_d = 35;
        g_cah_d = 4.5;
        g_h_d = 0.125;
    }

    void set_g_leak_d(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_leak_d.begin());
    }

    void set_g_leak_s(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_leak_s.begin());
    }

    void set_g_leak_a(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_leak_a.begin());
    }

    void set_g_kca_d(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_kca_d.begin());
    }

    void set_g_cah_d(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_cah_d.begin());
    }

    void set_g_h_d(py::array_t<double> v) {
        vex::copy(v.data(), v.data() + n, g_h_d.begin());
    }

    void operator()(const state_type &s, state_type &dsdt, double t) const {
        using namespace vex;

        auto v_dend  = tag<V_DEND >(s(V_DEND));
        auto v_soma  = tag<V_SOMA >(s(V_SOMA));
        auto v_axon  = tag<V_AXON >(s(V_AXON));
        auto r_d     = tag<R_D    >(s(R_D));
        auto z_d     = tag<Z_D    >(s(Z_D));
        auto n_d     = tag<N_D    >(s(N_D));
        auto ca_conc = tag<CA_CONC>(s(CA_CONC));
        auto k_s     = tag<K_S    >(s(K_S));
        auto l_s     = tag<L_S    >(s(L_S));
        auto h_s     = tag<H_S    >(s(H_S));
        auto n_s     = tag<N_S    >(s(N_S));
        auto x_s     = tag<X_S    >(s(X_S));
        auto h_a     = tag<H_A    >(s(H_A));
        auto x_a     = tag<X_A    >(s(X_A));

        auto Isd     = g_int / (1 - p1) * (v_dend - v_soma);
        auto Icah_d  = -g_cah_d * r_d * r_d * (Vcah_d - v_dend);
        auto Ikca_d  = -g_kca_d * z_d * (Vkca_d - v_dend);
        auto Ih_d    = -g_h_d * n_d * (Vh_d - v_dend);
        auto Ileak_d = -g_leak_d * (Vleak_d - v_dend);

        auto Ids     = g_int / p1 * (v_soma - v_dend);
        auto Ias     = g_int / (1 - p2) * (v_soma - v_axon);
        auto Ical_s  = -g_cal_s * k_s * k_s * k_s * l_s * (Vcal_s - v_soma);
        auto m_s     = make_temp<1>(1 / (1 + exp(-(v_soma + 30) / 5.5)));
        auto Ina_s   = -g_na_s * m_s * m_s * m_s * h_s * (Vna_s - v_soma);
        auto Ikdr_s  = -g_kdr_s * n_s * n_s * n_s * n_s * (Vkdr_s - v_soma);
        auto Ik_s    = -g_k_s * x_s * x_s * x_s * x_s * (Vk_s - v_soma);
        auto Ileak_s = -g_leak_s * (Vleak_s - v_soma);

        auto Isa     = g_int / p2 * (v_axon - v_soma);
        auto m_a     = make_temp<2>(1 / (1 + exp(-(v_axon + 30)/5.5)));
        auto Ina_a   = -g_na_a * m_a * m_a * m_a * h_a * (Vna_a - v_axon);
        auto Ik_a    = -g_k_a * x_a * x_a * x_a * x_a * (Vk_a - v_axon);
        auto Ileak_a = -g_leak_a * (Vleak_a - v_axon);

        dsdt = std::make_tuple(
                /*v_dend */ (Iapp - Isd - Icah_d - Ikca_d - Ih_d - Ileak_d) / Cm,

                /*v_soma */ (-Ids - Ias - Ical_s - Ina_s - Ikdr_s - Ik_s - Ileak_s) / Cm,

                /*v_axon */ (-Isa - Ina_a - Ik_a - Ileak_a) / Cm,

                /*r_d    */ 0.2 * 1.7 / (1 + exp(-(v_dend - 5)/13.9)) * (1 - r_d) -
                            0.2 * 0.1 * (v_dend + 8.5) / (-5) * r_d / (1 - exp((v_dend + 8.5)/5)),

                /*z_d    */ min(2e-5 * ca_conc, 1e-2) * (1 - z_d) - 0.015 * z_d,

                /*n_d    */ (1 / (1 + exp((v_dend + 80) / 4)) - n_d) *
                            (exp(-0.086 * v_dend - 14.6) + exp(0.070 * v_dend - 1.87)),

                /*ca_conc*/ -3 * Icah_d - 0.075 * ca_conc,

                /*k_s    */ 1 / (1 + exp(-(v_soma + 61)/4.2)) - k_s,

                /*l_s    */ (1 / (1 + exp((v_soma + 85.5) / 8.5)) - l_s) /
                            ((20 * exp((v_soma + 160) / 30) / (1 + exp((v_soma + 84)/7.3))) + 35),

                /*h_s    */ (1 / (1 + exp((v_soma + 70) / 5.8)) - h_s) /
                            (3 * exp(-(v_soma + 40) / 33)),

                /*n_s    */ (1 / (1 + exp(-(v_soma + 3) / 10)) - n_s) /
                            (5 + 47 * exp((v_soma + 50) / 900)),

                /*x_s    */ (1 - x_s) * (0.13 * (v_soma + 25)) / (1 - exp(-(v_soma + 25) / 10)) -
                            x_s * 1.69 * exp(-(v_soma + 35) / 80),

                /*h_a    */ (1 / (1 + exp((v_axon + 60) / 5.8)) - h_a) /
                            (1.5 * exp(-(v_axon + 40) / 33)),

                /*x_a    */ (1 - x_a) * (0.13 * (v_axon + 25)) / (1 - exp(-(v_axon + 25) / 10)) -
                            x_a * 1.69 * exp(-(v_axon + 35) / 80)
                );
    }

    void advance(py::array_t<double> state, int num_steps, double dt, double Iapp) {
        {
            const double *p = state.data();
            for(int i = 0; i < 14; ++i, p += n)
                vex::copy(p, p + n, s(i).begin());
        }

        this->Iapp = Iapp;

        for(int i = 0; i < num_steps; ++i, t += dt)
            stepper.do_step(std::ref(*this), s, t, dt);

        {
            double *p = state.mutable_data();
            for(int i = 0; i < 14; ++i, p += n)
                vex::copy(s(i).begin(), s(i).end(), p);
        }
    }
};

PYBIND11_MODULE(pybf, m) {
    m.attr("V_DEND")  = static_cast<int>(V_DEND);
    m.attr("V_SOMA")  = static_cast<int>(V_SOMA);
    m.attr("V_AXON")  = static_cast<int>(V_AXON);
    m.attr("R_D")     = static_cast<int>(R_D);
    m.attr("Z_D")     = static_cast<int>(Z_D);
    m.attr("N_D")     = static_cast<int>(N_D);
    m.attr("CA_CONC") = static_cast<int>(CA_CONC);
    m.attr("K_S")     = static_cast<int>(K_S);
    m.attr("L_S")     = static_cast<int>(L_S);
    m.attr("H_S")     = static_cast<int>(H_S);
    m.attr("N_S")     = static_cast<int>(N_S);
    m.attr("X_S")     = static_cast<int>(X_S);
    m.attr("H_A")     = static_cast<int>(H_A);
    m.attr("X_A")     = static_cast<int>(X_A);

    m.def("context", [](std::string name) {
            std::ostringstream s; s << ctx(name); py::print(s.str());
            }, py::arg("name") = std::string(""));

    py::class_<neuron_system>(m, "neuron_system")
        .def(py::init<int>())
        .def("advance", &neuron_system::advance)
        .def("set_g_leak_d", &neuron_system::set_g_leak_d)
        .def("set_g_leak_s", &neuron_system::set_g_leak_s)
        .def("set_g_leak_a", &neuron_system::set_g_leak_a)
        .def("set_g_kca_d",  &neuron_system::set_g_kca_d)
        .def("set_g_cah_d",  &neuron_system::set_g_cah_d)
        .def("set_g_h_d",    &neuron_system::set_g_h_d)
        ;
}
